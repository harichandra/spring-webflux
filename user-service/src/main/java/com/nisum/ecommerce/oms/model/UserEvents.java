package com.nisum.ecommerce.oms.model;


import java.util.Date;

public class UserEvents {

    private User user;
    private Date date;

    public UserEvents(User user, Date date) {
        this.user = user;
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "UserEvents{" +
                "user=" + user +
                ", date=" + date +
                '}';
    }
}
