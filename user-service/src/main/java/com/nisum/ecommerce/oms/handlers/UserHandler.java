package com.nisum.ecommerce.oms.handlers;

import com.nisum.ecommerce.oms.model.User;
import com.nisum.ecommerce.oms.model.UserEvents;
import com.nisum.ecommerce.oms.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Date;
import java.util.stream.Stream;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromPublisher;
import static org.springframework.web.reactive.function.server.ServerResponse.notFound;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class UserHandler {

    private UserRepository userRepository;

    public UserHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Mono<ServerResponse> createUser(ServerRequest request) {
        Mono<User> user = request.bodyToMono(User.class);
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(fromPublisher(user.flatMap(userRepository::save), User.class));
    }

    public Mono<ServerResponse> getUser(ServerRequest request) {

        final int id = Integer.parseInt(request.pathVariable("id"));
        final Mono<User> user = userRepository.findById(id);
        return user.flatMap(usr -> ok().contentType(APPLICATION_JSON)
                .body(fromPublisher(user, User.class)))
                .switchIfEmpty(notFound().build());
    }

    public Mono<ServerResponse> getUsers(ServerRequest request) {
        return ok().contentType(APPLICATION_JSON)
                .body(fromPublisher(userRepository.findAll(), User.class));
    }

    /*public Mono<ServerResponse> deleteUser(ServerRequest request) {

        final int userId = Integer.parseInt(request.pathVariable("id"));

        final Mono<User> user = userRepository.findById(userId);

        return user
                .flatMap(existingUser ->
                        userRepository.deleteById(existingUser.getId())
                                .flatMap(usr -> ok().contentType(APPLICATION_JSON)
                                .body(fromPublisher(user, User.class))
                                        .switchIfEmpty(notFound().build())
                                ));

    }*/

    /*public Mono<ServerResponse> deleteUser(ServerRequest request) {

        final int userId = Integer.parseInt(request.pathVariable("id"));

        final Mono<User> user = userRepository.findById(userId);

        return ServerResponse.ok()
                .contentType(APPLICATION_JSON)
                .body(user.flatMap(existingUser ->
                        userRepository.deleteById(existingUser.getId())), Void.class);
    }*/

    public Mono<ServerResponse> deleteUser(ServerRequest request) {

        final int userId = Integer.parseInt(request.pathVariable("id"));

        final Mono<User> user = userRepository.findById(userId);

        return userRepository.findById(userId)
                        .flatMap(existingUser ->
                                            userRepository.delete(existingUser)
                                            .then(ServerResponse.ok().<Void>build())
                             ).switchIfEmpty(notFound().build());
    }

    public Mono<ServerResponse> updateUser(ServerRequest request) {

       /* final int userId = Integer.parseInt(request.pathVariable("id"));
        final Mono<User> userRepositoryById = userRepository.findById(userId);*/

        Mono<User> userMono = request.bodyToMono(User.class);

       return ServerResponse.ok()
                .contentType(APPLICATION_JSON)
                .body(BodyInserters.fromPublisher(
                       userMono.flatMap(userRepository::save), User.class)
               ).switchIfEmpty(notFound().build());

    }

    public Mono<ServerResponse> getUserByEvents(ServerRequest serverRequest) {
        Integer userId = Integer.valueOf(serverRequest.pathVariable("id"));
        return  ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(
                        userRepository.findById(userId)
                                .flatMapMany(employee -> {
                                    Flux<Long> intervalFlux = Flux.interval(Duration.ofSeconds(2));

                                    Flux<UserEvents> employeeEventsFlux =
                                            Flux.fromStream(Stream.generate(()-> new UserEvents(employee, new Date())));

                                    return Flux.zip(intervalFlux,employeeEventsFlux).map(objects -> objects.getT2());
                                }), UserEvents.class
                );
    }

}
