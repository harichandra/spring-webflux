package com.nisum.ecommerce.oms;

import com.nisum.ecommerce.oms.model.User;
import com.nisum.ecommerce.oms.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@SpringBootApplication
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

    /*@Bean
    public CommandLineRunner user(UserRepository repository) {

        final User hendi = new User(1, "Hendi",  "Santika@gmail.com");
        final User naruto = new User(2, "Uzumaki", "Naruto@gmail.com");
        final User sasuke = new User(3, "Uchiha",  "Sasuke@gmail.com");
        final User sakura = new User(4, "Sakura", "Haruno@gmail.com");
        final User kakashi = new User(5, "Hatake", "Kakashi@gmail.com");

        repository.saveAll(Flux.just(hendi, naruto, sasuke, sakura, kakashi)).subscribe();
       // repository.findByName("hendi").log().map(User::getName).subscribe(System.out::println);
        //repository.findById("hendi").log().map(User::getId).subscribe(System.out::println);


        return (args) -> {
//			// save users
            repository.save(new User(1, "Hendi",  "Santika@gmail.com"));
            repository.save(new User(2, "Uzumaki", "Naruto@gmail.com"));
            repository.save(new User(3, "Uchiha",  "Sasuke@gmail.com"));
            repository.save(new User(4, "Sakura", "Haruno@gmail.com"));
            repository.save(new User(5, "Hatake", "Kakashi@gmail.com"));
        };
    }*/
}
