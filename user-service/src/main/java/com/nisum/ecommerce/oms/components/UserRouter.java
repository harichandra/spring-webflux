package com.nisum.ecommerce.oms.components;

import com.nisum.ecommerce.oms.handlers.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration
public class UserRouter {

    @Bean
    public RouterFunction<ServerResponse> routes(UserHandler userHandler) {
        return RouterFunctions
                .route(GET("/users/{id}").and(accept(APPLICATION_JSON)), userHandler::getUser)
                .andRoute(GET("/users").and(accept(APPLICATION_JSON)), userHandler::getUsers)
                .andRoute(POST("/users").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)), userHandler::createUser)
                .andRoute(PUT("/users/updateuser/{id}").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)), userHandler::updateUser)
                .andRoute(DELETE("/users/deleteuser/{id}"), userHandler::deleteUser)
                .andRoute(GET("/users/events").and(accept(APPLICATION_JSON)), userHandler::getUserByEvents);
    }

   /* @Bean
    public RouterFunction<ServerResponse> userRoutes(UserHandler userHandler) {

        return RouterFunctions.route()
                .path("/users", builder -> builder
                        .POST("", accept(APPLICATION_JSON), userHandler::createUser)
                        .GET("/{id}", accept(APPLICATION_JSON), userHandler::getUser)
                        .GET("", accept(APPLICATION_JSON), userHandler::getUsers))
                        .PUT("/updateuser/{id}", accept(APPLICATION_JSON), userHandler::updateUser)
                        .DELETE("/deleteuser/{id}", accept(APPLICATION_JSON), userHandler::deleteUser)
                        .GET("/{id}/events", accept(APPLICATION_JSON), userHandler::getUserByEvents)
                .build();
    }
*/
}
