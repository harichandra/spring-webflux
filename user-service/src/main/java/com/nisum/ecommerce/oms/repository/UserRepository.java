package com.nisum.ecommerce.oms.repository;

import com.nisum.ecommerce.oms.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserRepository extends ReactiveMongoRepository<User, Integer> {

    Flux<User> findAll();
    Mono<User> findById(int id);
    Mono<User> findByName(String name);
    Mono<User> save(User user);


}

