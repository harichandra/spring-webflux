package com.nisum.ecommerce.oms.controller;

import com.nisum.ecommerce.oms.model.User;
import com.nisum.ecommerce.oms.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestUserHandlerIntegration {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testCreateUserSuccess() {

        User user = User.builder().id(6).name("Chandra").email("Chandra@gmail.com").build();

        webTestClient.post().uri("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(user), User.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.name").isEqualTo("Chandra")
                .jsonPath("$.email").isEqualTo("Chandra@gmail.com");
    }

    @Test
    public void testCreateUserFailure() {

        User user = User.builder().id(6).name("Nisum").email("NisumTech@nisum.com").build();

        webTestClient.post().uri("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(user), User.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.name").isEqualTo("Technologies")
                 .jsonPath("$.email").isEqualTo("Technologies@nisum.com");
    }

    @Test
    public void getUserByIdSuccessTest() {

        User user = userRepository.save(new User(1,"Hello, World!", "test@gmail.com")).block();

        webTestClient.get()
                .uri("/users/{id}", Collections.singletonMap("id", user.getId()))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .consumeWith(response ->
                        Assertions.assertThat(response.getResponseBody()).isNotNull());
    }

    @Test
    public void getUserByIdErrorTest() {

        User user = userRepository.save(new User(12,"Hello, World!", "test@gmail.com")).block();

        webTestClient.get()
                .uri("/users/{id}", Collections.singletonMap("id", user.getName()))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .consumeWith(response ->
                        Assertions.assertThat(response.getResponseBody()).isNotNull());
    }

    @Test
    public void getAllUsersTest() {

        webTestClient.get().uri("/users")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(User.class);



        /*this.webTestClient.get().uri("/users").exchange().expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo("Hello, World!");*/

        /*this.webTestClient.get().uri("/users")
                 .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                //.expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(String.class)
                .isEqualTo("Hendi");
*/

        /*webTestClient.post().uri("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(User.builder().name("This is a Test Tweet").build()), User.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.text").isEqualTo("This is a Test Tweet");*/


/*
        FluxExchangeResult<User> result = this.webTestClient.get()
                .uri("/users")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(User.class);;
                //.expectBody(String.class).isEqualTo("Hendi");

        Flux<User> eventFlux = result.getResponseBody();

        StepVerifier.create(eventFlux)
                .expectNextCount(10)
                .thenCancel()
                .verify();
*/
    }

    @Test
    public void testUpdateUser() {

        User user = userRepository.save(User.builder().id(1).name("Hendi").email("Santika@gmail.com").build()).block();

       User newUserData = User.builder().id(1).name("Prasad Updated").email("PrasadUpdated@gmail.com").build();

        webTestClient.put()
                .uri("/users/updateuser/{id}", Collections.singletonMap("id", user.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(newUserData), User.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.name").isEqualTo("Prasad Updated");
    }

    @Test
    public void testDeleteUser() {

        User user = userRepository.save(User.builder().id(1).name("Hendi").email("Santika@gmail.com").build()).block();

        webTestClient.delete()
                .uri("/users/deleteuser/{id}", Collections.singletonMap("id",  user.getId()))
                .exchange()
                .expectStatus().isOk();
    }

}
