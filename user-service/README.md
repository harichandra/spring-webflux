# Spring Boot Webflux Reactive Mongo

This is a sample application that shows how to build a web application using
 - Spring Boot 2
 - Spring Webflux
 - Spring Reactive Data MongoDb

 
<br/>
Please see the following pages for more details
  
  - Spring Web Reactive <br/><a>http://docs.spring.io/spring-framework/docs/5.0.0.M1/spring-framework-reference/html/web-reactive.html</a>
  - Spring Data Reactive <br/><a>https://spring.io/blog/2016/11/28/going-reactive-with-spring-data</a>
  - Spring Functional Web Framework <br/><a>https://spring.io/blog/2016/09/22/new-in-spring-5-functional-web-framework</a>

## Running

In application.properties, configure appropriate values.
<br/>
<br/>
Run this using using the gradle wrapper included

```
./gradlew bootRun
```

 .route(GET("/users/{id}").and(accept(APPLICATION_JSON)), userHandler::getUser)
                .andRoute(GET("/users").and(accept(APPLICATION_JSON)), userHandler::getUsers)
                .andRoute(POST("/users").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)), userHandler::createUser)
                .andRoute(PUT("/users/updateuser/{id}").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)), userHandler::updateUser)
                .andRoute(DELETE("/users/deleteuser/{id}"), userHandler::deleteUser)

And then go to http://localhost:8080 to test the API's.

`http://localhost:8080/users/1`  :  Get the User by passing the attribute id using GET

`http://localhost:8080/users`  : Get the User Details using GET

`http://localhost:8080/users`  : Create the User using POST

`http://localhost:8080/users/updateuser/{id}`  : Update the User details using PUT

`http://localhost:8080/users/deleteuser/{id}` :  Delete the User by passing the attribute id using DELETE

